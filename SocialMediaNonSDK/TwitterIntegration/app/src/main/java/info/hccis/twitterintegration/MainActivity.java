package info.hccis.twitterintegration;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import info.hccis.twitterintegration.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final TextView textView = findViewById(R.id.txtLabel);
        final EditText shareMessage = findViewById(R.id.txtShare);
        Button btnShare = findViewById(R.id.bttnShare);

        btnShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                // string variable to receive text entered into textarea on page
                String message = shareMessage.getText().toString();
                // the intent "ACTION_SEND" will be used to send the information from one activity to another
                Intent intent = new Intent(Intent.ACTION_SEND);
                // set the type of input you will be sending to the application
                intent.setType("text/plain");

                PackageManager packageManager = getPackageManager();
                List<ResolveInfo> resolveInfoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

                boolean resolved = false;
                for (ResolveInfo resolveInfo : resolveInfoList) {
                    if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                        intent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
                        resolved = true;
                        break;
                    }
                }
                if (resolved) {
                    startActivity(intent);
                } else {
                    textView.setText("Error no app found");
                }
//                This isn't mandatory but a subject line can be added
                intent.putExtra(Intent.EXTRA_SUBJECT, "Fitness Center");
//                put the text that has been entered into the input
                intent.putExtra(Intent.EXTRA_TEXT, message);
//                This creates the dialogue menu pop up of the apps that the message can be shared using
                startActivity(intent.createChooser(intent, "Share message using"));


            }
        });
        Button btnTweet = findViewById(R.id.bttnTweet);
        btnTweet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String tweet = shareMessage.getText().toString();
//                variable to hold twitter intent to post a tweet
                String twitterUrl = String.format("https://twitter.com/intent/tweet?text=%s&url=%s",
                        urlEncode(tweet),
                        urlEncode(""));

                //                Send the user to twitter in web browser
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(twitterUrl));

                PackageManager packageManager = getPackageManager();
                List<ResolveInfo> resolveInfoList = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

                boolean resolved = false;
                for (ResolveInfo resolveInfo : resolveInfoList) {
                    if (resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")) {
                        intent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
                        resolved = true;
                        break;
                    }
                }
                if (resolved) {
                    startActivity(intent);
                } else {
                    textView.setText("Error no app found");
                }

//                this startActivity function allows the intent to be processed
                startActivity(intent);
            }
        });
        Button btnViewPage = findViewById(R.id.bttnViewPage);
        btnViewPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.twitter.com/hollandcollege/?hl=en"));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //this function allows the string to be encoded
    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }
}