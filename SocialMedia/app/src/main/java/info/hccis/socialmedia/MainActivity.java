package info.hccis.socialmedia;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.twitter.ParseTwitterUtils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseTwitterUtils.initialize(getString(R.string.twitter_consumer_key), getString(R.string.twitter_consumer_secret));

        Button loginButton = findViewById(R.id.button);
        loginButton.setOnClickListener(view -> ParseTwitterUtils.logIn(MainActivity.this, (user, e) -> {
            if (e != null) {
                ParseUser.logOut();
            }
            if (user == null) {
                ParseUser.logOut();
                Toast.makeText(MainActivity.this, "The user cancelled the Twitter login.", Toast.LENGTH_LONG).show();
            } else if (user.isNew()) {
                Toast.makeText(MainActivity.this, "User signed up and logged in through Twitter.", Toast.LENGTH_LONG).show();

                user.setUsername(ParseTwitterUtils.getTwitter().getScreenName());
                user.saveInBackground(e1 -> {
                    if (e1 == null) {
                        alertDisplay("Login", "Welcome!");
                    } else {
                        ParseUser.logOut();
                        Toast.makeText(MainActivity.this, "It was not possible to save your username.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Toast.makeText(MainActivity.this, "User logged in through Twitter.", Toast.LENGTH_LONG).show();
                alertDisplay("Oh, you!", "Welcome back!");
            }
        }));
    }

    private void alertDisplay(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialog, which) -> {
                    dialog.cancel();
                    // don't forget to change the line below with the names of your Activities
                    Intent intent = new Intent(MainActivity.this, LogoutActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                });
        AlertDialog ok = builder.create();
        ok.show();
    }
}